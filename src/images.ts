import { Image } from './interfaces/image'

export const images: Record<string, Image> = {
  logo_main: {
    size: [400, 272]
  },
  logo_small: {
    size: [108, 108]
  },
  logo: {
    size: [130, 100]
  },
  logo_medium: {
    size: [240, 84]
  },
  logo_min: {
    size: [84, 84]
  },
  logo_dp: {
    size: [174, 109]
  },
  home_page: {
    size: [18, 18]
  }
}
