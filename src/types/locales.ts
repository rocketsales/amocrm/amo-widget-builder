export type Locales = 'ru' | 'en' | 'es'

export type LocalesObject<T = any> = Partial<Record<Locales, T>>
