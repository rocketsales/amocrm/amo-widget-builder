export { Config } from './interfaces/config'
export { Manifest } from './interfaces/manifest'
export { WidgetBuilder } from './widget-builder'
export { defaultManifest } from './default-manifest'
