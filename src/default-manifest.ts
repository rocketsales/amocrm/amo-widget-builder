import { Manifest } from './interfaces/manifest'
import { DeepPartial } from './types/deep-partial'

export const defaultManifest: DeepPartial<Manifest> = {
  widget: {
    version: '1.0.0',
    interface_version: 2,
    init_once: true,
    installation: true,
    support: {
      link: 'https://rocketsales.ru',
      email: 'support@rocketsals.ru'
    }
  },
  locations: ['settings', 'everywhere'],
  tour: {
    is_tour: true
  },
  settings: {
    fakeConfig: {
      type: 'custom',
      required: true
    }
  }
}
