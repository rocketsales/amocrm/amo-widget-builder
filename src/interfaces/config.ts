import { Manifest, Setting } from './manifest'
import { DeepPartial } from '../types/deep-partial'
import { Locales, LocalesObject } from '../types/locales'

export interface Config {
  name?: string | LocalesObject<string>
  version?: string
  description?: string | LocalesObject<string>
  shortDescription?: string | LocalesObject<string>
  tourDescription?: string | LocalesObject<string>
  advancedSettingsTitle?: string | LocalesObject<string>
  locales?: LocalesObject<object | boolean> | Locales[]
  locations?: string[]
  manifest?: DeepPartial<Manifest>
  fakeConfig?: Partial<Setting>
  fakeImages?: {
    colors?: {
      background?: string
      text?: string
    }
  }
  debugKey?: string
  imagesDir?: string
  bundleDir?: string
  entryPoint?: string
  copyBundle?: boolean
  outDir?: string
  cleanOnFinish?: boolean
  disableAdvancedSettings?: boolean
}
