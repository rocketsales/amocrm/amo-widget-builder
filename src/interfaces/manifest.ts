import { Locales } from '../types/locales'

export interface Manifest {
  widget: Widget
  locations: string[]
  tour: Tour
  settings: Settings
  advanced?: {
    title: string
  }
  dp?: {
    settings: Settings
    action_multiple: boolean
    webhook_url: string
  }
  left_menu?: {
    stats?: Submenu | IsHidden
    settings?: Submenu | IsHidden
    [widgetCode: string]:
      | ({
          title: string
          icon: string
          sort?: {
            after: 'dashboard' | 'leads' | 'customers' | 'tasks' | 'catalogs' | 'mail' | 'stats' | 'settings'
          }
        } & Submenu)
      | Submenu
      | IsHidden
      | undefined
  }
}

export interface Widget {
  name: string
  description: string
  short_description: string
  version: string
  interface_version: number
  init_once: boolean
  locale: Locales[]
  installation: boolean
  support: {
    link: string
    email: string
  }
}

export interface Tour {
  is_tour: true
  tour_images: {
    [key in Locales]: string[]
  }
  tour_description: string
}

export interface Settings {
  [key: string]: Setting
}

export interface Setting {
  name: string
  type: 'text' | 'password' | 'custom'
  required: boolean
}

interface Submenu {
  submenu: {
    [key: string]: {
      title: string
      sort?: number
    }
  }
}

interface IsHidden {
  is_hidden: true
}
