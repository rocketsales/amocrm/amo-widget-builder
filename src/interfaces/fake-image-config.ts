import { ImageSize } from '../types/image-size'

export interface FakeImageConfig {
  size: ImageSize
  text?: string
}
