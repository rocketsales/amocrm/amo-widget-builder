import { ImageSize } from '../types/image-size'

export interface Image {
  size: ImageSize
}
