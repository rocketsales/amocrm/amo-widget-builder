import * as path from 'path'
import * as fse from 'fs-extra'
import * as _ from 'lodash'
import * as async from 'async'
import logger from 'consola'
import axios, { AxiosInstance } from 'axios'
import globby from 'globby'
import JSZip from 'jszip'
import uniqid from 'uniqid'
import { Config } from './interfaces/config'
import { defaultManifest } from './default-manifest'
import { Locales, LocalesObject } from './types/locales'
import { images } from './images'
import { FakeImageConfig } from './interfaces/fake-image-config'
import { DeepPartial } from './types/deep-partial'
import { Manifest } from './interfaces/manifest'

export class WidgetBuilder {
  constructor(private config: Config = {}) {
    this.debugKey = `rsDebugUrl_${this.config.debugKey ?? uniqid()}`
  }

  async init() {
    try {
      await this.clean()
      await this.createTempFolder()
      await this.mergeManifest()
      await this.prepareImages()
      await this.prepareLocalesFiles()
      await this.prepareManifest()
      await this.prepareScript()
      await this.prepareBundle()
      await this.zip()

      if (this.cleanOnFinish) await this.clean()
    } catch (e) {
      logger.error(e)
    }
  }

  private manifest: DeepPartial<Manifest> = {}

  private readonly debugKey: string

  get bundleDir(): string {
    if (!this.config.bundleDir) {
      throw new Error('Не указана директория с бандлом')
    }

    return this.config.bundleDir
  }

  get entryPoint(): string {
    return this.config.entryPoint || 'bundle.js'
  }

  get entryPointIsURL(): boolean {
    return !!this.entryPoint.match(/^https?:\/\//)
  }

  get cleanOnFinish(): boolean {
    const { cleanOnFinish } = this.config
    return cleanOnFinish === undefined ? true : cleanOnFinish
  }

  private get sourceImagesDir() {
    return this.config.imagesDir || null
  }

  private get localesKeys(): Locales[] {
    return Array.isArray(this.locales) ? [...this.locales] : (Object.keys(this.locales) as Locales[])
  }

  private get locales(): Locales[] | LocalesObject<object | boolean> {
    return _.get(this.config, ['locales'], ['ru'])
  }

  private get fakeImageConnector(): AxiosInstance {
    return axios.create({
      baseURL: 'https://fakeimg.pl',
      responseType: 'stream'
    })
  }

  private get fakeImageColors(): { text: string; background: string } {
    return {
      text: (this.config.fakeImages?.colors?.text ?? 'fff').replace('#', ''),
      background: (this.config.fakeImages?.colors?.background ?? '088bc4').replace('#', '')
    }
  }

  async clean() {
    await fse.remove(this.tempWidgetPath)
    logger.success('Временная папка удалена')
  }

  async zip(): Promise<string> {
    const zip = new JSZip()
    const filePaths = await globby(['**', '!*.zip'], { cwd: this.tempWidgetPath })

    filePaths.forEach(filePath => {
      zip.file(filePath, fse.readFile(path.resolve(this.tempWidgetPath, filePath)))
    })

    const archive = (await zip.generateAsync({ type: 'nodebuffer' })) as Buffer
    await fse.writeFile(this.archivePath, archive)
    const stats = await fse.stat(this.archivePath)
    logger.info('Путь до архива', this.archivePath)
    logger.info('Размер архива', `${Math.round(stats.size / 1024)} KB`)
    logger.success('Архив готов')

    return this.archivePath
  }

  mergeManifest() {
    Object.keys(this.config.manifest?.settings || {}).forEach(settingKey => {
      if (_.get(defaultManifest, ['settings', settingKey])) {
        delete defaultManifest.settings![settingKey]
      }
    })

    if (!this.config.disableAdvancedSettings) {
      _.set(this.config, 'disableAdvancedSettings', false)
    }

    if (this.config.fakeConfig) {
      _.merge(defaultManifest.settings!.fakeConfig, this.config.fakeConfig)
    }

    this.manifest = _.mergeWith(defaultManifest, this.config.manifest, (value, srcValue) => {
      if (Array.isArray(srcValue)) return srcValue
    })

    if (Array.isArray(this.config.locations)) {
      this.manifest.locations = this.config.locations
    }

    if (this.config.advancedSettingsTitle) {
      const { locations } = this.manifest

      if (!locations!.includes('advanced_settings')) {
        locations!.push('advanced_settings')
      }

      if (!this.manifest.advanced) {
        this.manifest.advanced = {
          title: 'advanced.title'
        }
      }
    }

    _.set(this.manifest, ['widget', 'locale'], [...this.localesKeys])
  }

  async prepareManifest() {
    await fse.writeJson(path.resolve(this.tempWidgetPath, 'manifest.json'), this.manifest)
    logger.success('Манифест готов')
  }

  async prepareImages() {
    await this.createImagesFolder()

    const imagesKeys = Object.keys(images)
    await async.eachSeries(imagesKeys, async imageKey => {
      const ext = imageKey === 'home_page' ? 'svg' : 'png'
      const imagePath = this.sourceImagesDir && path.resolve(this.sourceImagesDir, `${imageKey}.${ext}`)
      const buffer = await this.getOrGenerateImage(imagePath, { size: images[imageKey].size, text: imageKey })

      return fse.writeFile(path.resolve(this.imagesPath, `${imageKey}.${ext}`), buffer)
    })

    await async.eachSeries(this.localesKeys, async localeKey => {
      let tourImagesPaths: string[] = []

      if (this.sourceImagesDir) {
        tourImagesPaths = await globby(
          [`tour/${localeKey}/**.png`, `tour/${localeKey}/**.jpg`, `tour/${localeKey}/**.gif`],
          {
            cwd: path.resolve(this.sourceImagesDir)
          }
        )
      }

      if (!tourImagesPaths.length) {
        tourImagesPaths = _.times(3, i => `tour/${localeKey}/tour_${i + 1}.png`)
      }

      _.set(
        this.manifest,
        ['tour', 'tour_images', localeKey],
        tourImagesPaths.map(path => `/images/${path}`)
      )

      await fse.mkdir(path.resolve(this.tourImagesPath, localeKey), { recursive: true })
      await async.eachOfSeries(tourImagesPaths, async (tourImagePath, index) => {
        const imagePath = this.sourceImagesDir && path.resolve(this.sourceImagesDir, tourImagePath)
        const buffer = await this.getOrGenerateImage(imagePath, {
          size: [1188, 616],
          text: `tour_${(index as number) + 1}`
        })

        return fse.writeFile(path.resolve(this.imagesPath, tourImagePath), buffer)
      })
    })

    logger.success('Изображения готовы')
  }

  async getOrGenerateImage(imagePath: string | null | undefined, fakeConfig: FakeImageConfig): Promise<Buffer> {
    const exists = imagePath && (await fse.pathExists(imagePath))
    if (exists) {
      return fse.readFile(imagePath as string)
    } else {
      return this.getFakeImage(fakeConfig)
    }
  }

  async getFakeImage(fakeConfig: FakeImageConfig): Promise<Buffer> {
    const {
      size: [width, height],
      text
    } = fakeConfig
    const params: any = {}
    if (text) params.text = text

    const colors = this.fakeImageColors
    const response = await this.fakeImageConnector.get(`/${width}x${height}/${colors.background}/${colors.text}/`, {
      params
    })

    const chunks: Buffer[] = []
    await new Promise(resolve => {
      response.data.on('data', (chunk: Buffer) => chunks.push(chunk))
      response.data.on('end', resolve)
    })

    return Buffer.concat(chunks)
  }

  async prepareLocalesFiles() {
    await this.createLocalesFolder()
    await async.each(this.localesKeys, async localeKey => {
      const locale = Array.isArray(this.locales) ? true : this.locales[localeKey]!
      const data: object = typeof locale === 'boolean' && locale ? {} : (locale as object)

      ;[
        ['name', 'widget.name'],
        ['description', 'widget.description'],
        ['shortDescription', 'widget.short_description'],
        ['tourDescription', 'tour.tour_description'],
        ['advancedSettingsTitle', 'advanced.title']
      ].forEach(([configProp, manifestProp]) => {
        if (manifestProp === 'advanced.title' && _.get(this.config, 'disableAdvancedSettings')) return
        const configValue = _.get(this.config, configProp)
        let localePath = _.get(this.manifest, manifestProp)
        if (!localePath) {
          localePath = manifestProp
          _.set(this.manifest, manifestProp, localePath)
        }

        let value

        if (typeof configValue === 'string') {
          value = configValue
        } else if (typeof configValue === 'object') {
          value = _.get(configValue, localeKey)
        }

        _.set(data, localePath, value || `Empty ${configProp}`)
      })

      const { settings = {} } = this.manifest
      Object.keys(settings).forEach(settingKey => {
        const setting: object = settings[settingKey]!
        let name = _.get(setting, 'name')
        if (!name) {
          name = `settings.${settingKey}`
          _.set(setting, 'name', name)
        }

        const locale = _.get(data, name)
        if (!locale) _.set(data, name, settingKey)
      })

      return await fse.writeJson(path.resolve(this.localesPath, `${localeKey}.json`), data)
    })

    logger.success('Файлы локализации готовы')
  }

  async prepareScript() {
    const scriptFile = await fse.readFile(path.resolve(__dirname, '../static/script.js'), 'utf8')
    const entrypoint = this.entryPointIsURL
      ? this.entryPoint
      : await (async () => {
          let filePath = this.entryPoint
          const exist = await fse.pathExists(path.resolve(this.bundleDir, this.entryPoint))

          if (!exist) {
            logger.info(`Входная точка не найдена, попытка найти файл с хешем`)

            const split = this.entryPoint.split('.')
            split.splice(split.length - 1, 0, '.+?')
            const filePaths = await globby(['*.js', '**/*.js'], { cwd: this.bundleDir })
            const regEx = new RegExp(`^${split.join('\\.')}$`)
            const found = filePaths.find(path => path.match(regEx))
            if (found) {
              logger.info(`Входная точка с хешем найдена`)
              filePath = found
            } else {
              throw new Error('Входная точка не найдена')
            }
          }

          return `./build/${filePath}`
        })()
    logger.info('Входная точка', entrypoint)

    const localStoragePlace = `localStorage['${this.debugKey}']`
    await fse.writeFile(
      path.resolve(this.tempWidgetPath, 'script.js'),
      scriptFile.replace('@entrypoint@', `\${${localStoragePlace} || '${entrypoint}'}`)
    )
    logger.success('Скрипт готов')
    logger.info(`Для дебага использовать ${localStoragePlace}`)
  }

  async prepareBundle() {
    if (this.entryPointIsURL) {
      return logger.info('Бандл не скопирован, так как входная точка - URL')
    }

    await fse.copy(this.bundleDir, path.resolve(this.tempWidgetPath, 'build'))
    logger.success('Бандл готов')
  }

  async createLocalesFolder() {
    return this.createFolder(this.localesPath)
  }

  async createImagesFolder() {
    return this.createFolder(this.imagesPath)
  }

  async createTempFolder() {
    return this.createFolder(this.tempWidgetPath)
  }

  async createFolder(path: string) {
    const exists = await fse.pathExists(path)
    if (!exists) await fse.mkdir(path)
  }

  private get localesPath(): string {
    return path.resolve(this.tempWidgetPath, 'i18n')
  }

  private get imagesPath(): string {
    return path.resolve(this.tempWidgetPath, 'images')
  }

  private get tourImagesPath(): string {
    return path.resolve(this.imagesPath, 'tour')
  }

  private get tempWidgetPath(): string {
    return path.resolve(__dirname, '../widget-temp')
  }

  private get archivePath(): string {
    return path.resolve(this.config.outDir || path.resolve(this.tempWidgetPath, '..'), 'widget.zip')
  }
}
